//
//  TimelineViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift
import RxSwiftUtilities

class TimelineViewController: BaseViewController, UITableViewDataSource, UITableViewDelegate {

    private static let TimelineTableContentTopInset: CGFloat = 20.0
    
    private var searchBar: UISearchBar!
    private var rightBarButtonItem: UIBarButtonItem!
    @IBOutlet private weak var timelineTableView: UITableView!
    @IBOutlet private weak var timelineActivityIndicatorView: UIActivityIndicatorView!
    
    private var filterViewModel: FilterCategoryViewModel!
    private var fullPhotoViewModel: FullscreenPhotoViewModel!
    private var activityIndicator: ActivityIndicator!
    private weak var viewModel: TimelineViewModel!
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: TimelineViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.bindViewModel()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.searchBar.text = self.searchBar.text
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI logic methods
    
    private func setupViews() {
        self.setupSearchBar()
        self.setupTableView()
    }
    
    private func setupSearchBar() {
        self.searchBar = UISearchBar(frame: CGRect.zero)
        self.searchBar.sizeToFit()
        self.searchBar.enablesReturnKeyAutomatically = false
        self.navigationItem.titleView = searchBar
        self.rightBarButtonItem = UIBarButtonItem(title: NSLocalizedString("Category", comment: ""), style: .plain, target: nil, action: nil)
        self.navigationItem.rightBarButtonItem = self.rightBarButtonItem
    }
    
    private func setupTableView() {
        self.edgesForExtendedLayout = []
        self.timelineTableView.dataSource = self
        self.timelineTableView.delegate = self
        self.timelineTableView.contentInset = UIEdgeInsets(top: TimelineViewController.TimelineTableContentTopInset, left: 0.0, bottom: 0.0, right: 0.0)
        self.timelineTableView.backgroundView = nil;
        self.timelineTableView.backgroundColor = UIColor.white
        self.timelineTableView.register(UINib(nibName: TimelineTableViewCell.className, bundle: Bundle.main), forCellReuseIdentifier: TimelineTableViewCell.className)
        self.timelineTableView.register(UINib(nibName: TimelineTableSectionHeaderView.className, bundle: Bundle.main), forHeaderFooterViewReuseIdentifier: TimelineTableSectionHeaderView.className)
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bindActivityExecuting()
        self.bindFilteredCategoriesViewModel()
        self.bindTableAppearance()
        self.bindSearchViewModel()
        self.bindSearchResults()
        self.bindFullPhotoAppearing()
    }

    private func bindActivityExecuting() {
        self.activityIndicator = ActivityIndicator()
        let executingDriver = activityIndicator.asDriver()
            .map { (isExecuting) -> Bool in
                return !isExecuting
        }
        executingDriver.drive(self.timelineActivityIndicatorView.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        executingDriver.drive(self.view.rx.isUserInteractionEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindFilteredCategoriesViewModel() {
        self.rightBarButtonItem
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { sender in
                self.filterViewModel = FilterCategoryViewModel(filteredCategories: self.viewModel.filteredCategories)
                let filterCategoryViewController = FilterCategoryViewController(nibName: FilterCategoryViewController.className, bundle: nil, viewModel: self.filterViewModel)
                self.present(filterCategoryViewController, animated: true)
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindSearchViewModel() {
        let filteredCategoriesChangedObservable = NotificationCenter.default
            .rx
            .notification(FilterCategoryViewController.FilteredCategoriesChangedNotification)
            .map { notification -> String in
                self.viewModel.filteredCategories = notification.object as! Set<Photo.Category>
                self.dismiss(animated: true)
                return self.searchBar.text ?? ""
        }
        let searchBarTextChangedObservable = self.searchBar
            .rx
            .textDidChange
            .map { (searchBar: UISearchBar, searchText: String) -> String in
                return searchText
        }
        let searchBarButtonClickedObservable = self.searchBar
            .rx
            .searchBarSearchButtonClicked.map { searchBar -> String in
                searchBar.resignFirstResponder()
                return searchBar.text!
        }
        let searchBarInitialTextObservable = Observable<String>.just(self.searchBar.text ?? "")
        let networkReachableObservable = AppDelegate.shared()
            .reachableObservable()
            .map { isReachable -> String in
                if !isReachable {
                    self.presentSettingsAlertWith(message: NSLocalizedString("NoInternetConnection", comment: ""))
                }
                return self.searchBar.text ?? ""
        }
        let photoAddedNotificationObservable = NotificationCenter.default
            .rx
            .notification(MapViewController.PhotoAddedNotification)
            .map { notification -> String in
                return self.searchBar.text!
        }
        Observable<String>.merge([searchBarTextChangedObservable, searchBarButtonClickedObservable, searchBarInitialTextObservable, filteredCategoriesChangedObservable, photoAddedNotificationObservable, networkReachableObservable])
            .filter { query -> Bool in
                return AppDelegate.shared().reachability.isReachable
            }
            .flatMapLatest { query -> Observable<[Photo]> in
                self.viewModel.searchPhotos(withSearchText: query).trackActivity(self.activityIndicator)
            }
            .subscribe(onNext: { photos in
                self.viewModel.photos.value = photos
            }, onError: { error in
                self.presentAlertWithMessage(error.localizedDescription)
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindSearchResults() {
        self.viewModel
            .sectionsChangedObservable
            .subscribe(onNext: { sections in
                self.viewModel.sections = sections
                self.timelineTableView.setContentOffset(CGPoint.zero, animated: false)
                self.timelineTableView.reloadData()
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindTableAppearance() {
        self.timelineTableView
            .rx
            .willDisplayCellForRowAtIndexPath
            .subscribe(onNext: { (tableView: UITableView, cell: UITableViewCell, indexPath: IndexPath) in
                if cell.responds(to: #selector(getter: UITableView.separatorInset)) {
                    cell.separatorInset = UIEdgeInsets.zero
                }
                if cell.responds(to: #selector(getter: UITableView.preservesSuperviewLayoutMargins)) {
                    cell.preservesSuperviewLayoutMargins = false
                }
                if cell.responds(to: #selector(getter: UITableView.layoutMargins)) {
                    cell.layoutMargins = UIEdgeInsets.zero
                }
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindFullPhotoAppearing() {
        self.timelineTableView
            .rx
            .didSelectRowAtIndexPath
            .subscribe(onNext: { (tableView: UITableView, indexPath: IndexPath) in
                tableView.deselectRow(at: indexPath, animated: true)
                self.fullPhotoViewModel = FullscreenPhotoViewModel(photo: self.viewModel.getPhoto(forIndexPath: indexPath))
                let fullPhotoViewController = FullscreenPhotoViewController(nibName: FullscreenPhotoViewController.className, bundle: nil, viewModel: self.fullPhotoViewModel)
                self.presentHorizontally(viewController: fullPhotoViewController, animated: false)
            })
            .addDisposableTo(self.disposeBag)
    }
    
    // MARK: - UITableViewDataSource
    
    public func numberOfSections(in tableView: UITableView) -> Int {
        return self.viewModel.sections.count
    }
    
    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.viewModel.sections[section].size
    }
    
    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: TimelineTableViewCell.className) as! TimelineTableViewCell
        let photo = self.viewModel.getPhoto(forIndexPath: indexPath)
        cell.bindPhoto(photo)
        
        return cell
    }
    
    // MARK: - UITableViewDelegate
    
    public func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let cell = tableView.dequeueReusableCell(withIdentifier: TimelineTableViewCell.className) as! TimelineTableViewCell
        let photo = self.viewModel.getPhoto(forIndexPath: indexPath)
        cell.bindPhoto(photo)
        
        return cell.frame.size.height
    }
    
    public func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:TimelineTableSectionHeaderView.className)
        return view!.bounds.size.height
    }
    
    public func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = tableView.dequeueReusableHeaderFooterView(withIdentifier:TimelineTableSectionHeaderView.className) as! TimelineTableSectionHeaderView
        let photo = self.viewModel.getPhoto(forIndexPath: IndexPath(row: 0, section: section))
        view.bindPhoto(photo)
        
        return view
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
