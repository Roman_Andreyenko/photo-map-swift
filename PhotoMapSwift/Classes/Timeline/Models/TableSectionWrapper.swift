//
//  TableSectionWrapper.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

class TableSectionWrapper: NSObject {
    
    var startIndex: Int!
    var size: Int!
    
    override init() {
        super.init()
        
        self.startIndex = 0
        self.size = 0
    }
    
    init(startIndex: Int, size: Int) {
        super.init()
        
        self.startIndex = startIndex
        self.size = size
    }
    
}
