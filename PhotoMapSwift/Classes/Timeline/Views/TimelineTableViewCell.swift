//
//  TimelineTableViewCell.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

class TimelineTableViewCell: UITableViewCell {

    @IBOutlet private weak var descriptionTextView: UILabel!
    @IBOutlet private weak var dateAndCategoryView: UILabel!
    @IBOutlet private weak var photoView: UIImageView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    private var viewModel: TimelineTableViewCellViewModel!
    
    private let disposeBag = DisposeBag()

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.activityIndicatorView.startAnimating()
        self.photoView.image = nil
    }
    
    private func bindViewModel(withPhoto photo: Photo) {
        self.photoView.image = nil
        self.viewModel = TimelineTableViewCellViewModel(photo: photo, takeDownloadUntil: self.rx.sentMessage(#selector(UITableViewCell.prepareForReuse)))
        self.viewModel.descriptionTextHiddenObservable.bindTo(self.descriptionTextView.rx.isHidden).addDisposableTo(self.disposeBag)
        self.viewModel.descriptionTextObservable.bindTo(self.descriptionTextView.rx.text).addDisposableTo(self.disposeBag)
        self.viewModel.dateAndCategoryTextObservable.bindTo(self.dateAndCategoryView.rx.text).addDisposableTo(self.disposeBag)
        self.viewModel
            .photoThumbnailObservable
            .subscribeOn(MainScheduler.instance)
            .subscribe(onNext: { image in
                self.photoView.image = image
                self.activityIndicatorView.stopAnimating()
            }, onError: { error in
                self.activityIndicatorView.stopAnimating()
            }, onCompleted: {
                self.activityIndicatorView.stopAnimating()
            })
            .addDisposableTo(self.disposeBag)
    }
    
    public func bindPhoto(_ photo: Photo) {
        if (self.viewModel == nil) {
            self.bindViewModel(withPhoto: photo)
        } else {
            self.viewModel.photo.value = photo
        }
    }
    
}
