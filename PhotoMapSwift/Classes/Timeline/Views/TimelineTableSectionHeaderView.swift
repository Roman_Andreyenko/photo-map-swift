//
//  TimelineTableSectionHeaderView.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class TimelineTableSectionHeaderView: UITableViewHeaderFooterView {

    @IBOutlet private weak var headerView: UILabel!
    
    private var viewModel: TimelineTableSectionHeaderViewModel!
    
    private let disposeBag = DisposeBag()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.bindViewModel()
    }
    
    private func bindViewModel() {
        self.viewModel = TimelineTableSectionHeaderViewModel()
        self.viewModel
            .headerTextObservable
            .bindTo(self.headerView.rx.text)
            .addDisposableTo(self.disposeBag)
    }
    
    public func bindPhoto(_ photo: Photo) {
        self.viewModel.photo.value = photo
    }
}
