//
//  TimelineTableSectionHeaderViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

class TimelineTableSectionHeaderViewModel: NSObject {

    var photo: Variable<Photo?>!
    var headerTextObservable: Observable<String>!
    
    override init() {
        super.init()
        
        self.initialize()
    }
    
    private func initialize() {
        self.photo = Variable<Photo?>(nil)
        self.headerTextObservable = self.photo.asObservable().map { photo -> String in
            return photo != nil
                ? DateManager.sharedInstance.MMMM_yyyy.string(from: photo!.createdTime)
                : ""
        }
    }
}
