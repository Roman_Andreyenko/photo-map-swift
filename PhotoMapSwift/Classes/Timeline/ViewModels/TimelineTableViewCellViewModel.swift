//
//  TimelineTableViewCellViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift

class TimelineTableViewCellViewModel: NSObject {

    var photo: Variable<Photo>!
    var descriptionTextObservable: Observable<String>!
    var dateAndCategoryTextObservable: Observable<String>!
    var descriptionTextHiddenObservable: Observable<Bool>!
    var photoThumbnailObservable: Observable<UIImage?>!
    
    init(photo: Photo, takeDownloadUntil: Observable<[Any]>) {
        super.init()
        
        self.initialize(withPhoto: photo, takeDownloadUntil: takeDownloadUntil)
    }
    
    private func initialize(withPhoto photo: Photo, takeDownloadUntil: Observable<[Any]>) {
        self.photo = Variable<Photo>(photo)
        self.descriptionTextObservable = self.photo.asObservable().map { photo -> String in
            return photo.descriptionText
        }
        self.dateAndCategoryTextObservable = self.photo.asObservable().map { photo -> String in
            return String(format: "%@ / %@", DateManager.sharedInstance.MM_dd_yyyy.string(from: photo.createdTime), photo.category.rawValue.uppercased())
        }
        self.descriptionTextHiddenObservable = self.photo.asObservable().map { photo -> Bool in
            return photo.descriptionText.isEmpty
        }
        self.photoThumbnailObservable = self.photo
            .asObservable()
            .observeOn(ConcurrentDispatchQueueScheduler(qos: .background))
            .flatMapLatest { photo -> Observable<UIImage?> in
                return photo.downloadPictureThumbnailObservable().takeUntil(takeDownloadUntil).map { data -> UIImage? in
                    return UIImage(data: data)
            }
        }
    }
    
}
