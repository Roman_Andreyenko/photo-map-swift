//
//  TimelineViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import Action
import FirebaseDatabase
import FirebaseAuth

class TimelineViewModel: NSObject {

    var photos: Variable<[Photo]>!
    var sections: [TableSectionWrapper]!
    var sectionsChangedObservable: Observable<[TableSectionWrapper]>!
    var filteredCategories: Set<Photo.Category>!
    
    override init() {
        super.init()
        
        self.initialize()
    }
    
    private func initialize() {
        self.photos = Variable([Photo]())
        self.sections = [TableSectionWrapper]()
        self.sectionsChangedObservable = calculateSections()
        self.filteredCategories = [.Nature, .Friends, .Default]
    }
    
    public func searchPhotos(withSearchText searchText: String) -> Observable<[Photo]> {
        let tags = Hashtag.parseTagsInText(searchText)
        return tags.count > 0 ? self.searchPhotosWithHashtags(tags) : self.searchPhotosWithoutHashtags()
    }

    public func getPhoto(forIndexPath indexPath: IndexPath) -> Photo {
        let section = self.sections[indexPath.section]
        return self.photos.value[section.startIndex + indexPath.row]
    }
    
    private func searchPhotosWithHashtags(_ tags: [String]) -> Observable<[Photo]> {
        return self.searchHashtagKeys(tags)
            .flatMapLatest { hashtagKeys -> Observable<Set<String>> in
            return self.searchPhotoIndexKeys(hashtagKeys)
        }.flatMapLatest { photoKeys -> Observable<[Photo]> in
            return self.searchFilteredPhotos(photoKeys)
        }
    }
    
    private func searchPhotosWithoutHashtags() -> Observable<[Photo]> {
        return Photo.readByOwnerId(FIRAuth.auth()!.currentUser!.uid).map { photos -> [Photo] in
            var filteredPhotos = [Photo]()
            for photo in photos {
                if self.filteredCategories.contains(photo.category) {
                    filteredPhotos.append(photo)
                }
            }
            return filteredPhotos
        }
    }
    
    private func searchHashtagKeys(_ tags: [String]) -> Observable<Set<String>> {
        return Hashtag.readAll().map { allHashtags -> Set<String> in
            let tagSet = Set<String>(tags)
            var filteredHashtagKeys = Set<String>()
            for hashtag in allHashtags {
                if tagSet.contains(hashtag.hashtag) {
                    filteredHashtagKeys.insert(hashtag.key!)
                }
            }
            return filteredHashtagKeys
        }
    }
    
    private func searchPhotoIndexKeys(_ hashtagKeys: Set<String>) -> Observable<Set<String>> {
        return PhotoIndex.readAll().map { photoIndexes -> Set<String> in
            var filteredPhotoKeys = Set<String>()
            for photoIndex in photoIndexes {
                if hashtagKeys.contains(photoIndex.hashtagKey) {
                    filteredPhotoKeys.insert(photoIndex.photoKey)
                }
            }
            return filteredPhotoKeys
        }
    }
    
    private func searchFilteredPhotos(_ photoKeys: Set<String>) -> Observable<[Photo]> {
        return Photo.readByOwnerId(FIRAuth.auth()!.currentUser!.uid).map { photos -> [Photo] in
            var filteredPhotos = [Photo]()
            for photo in photos {
                if self.filteredCategories.contains(photo.category)
                    && photoKeys.contains(photo.key!) {
                    filteredPhotos.append(photo)
                }
            }
            return filteredPhotos
        }
    }
    
    private func calculateSections() -> Observable<[TableSectionWrapper]> {
        return self.photos.asObservable().map { photos -> [TableSectionWrapper] in
            var currentMonth = 0
            var currentSectionWrapper = TableSectionWrapper(startIndex: 0, size: 0)
            var sections = [TableSectionWrapper]()
            for i in 0..<photos.count {
                let photo = photos[i]
                let month = Calendar.current.component(.month, from: photo.createdTime)
                if currentMonth != month {
                    currentMonth = month
                    currentSectionWrapper.size = i - currentSectionWrapper.startIndex
                    currentSectionWrapper = TableSectionWrapper(startIndex: i, size: 0)
                    sections.append(currentSectionWrapper)
                }
            }
            currentSectionWrapper.size = photos.count - currentSectionWrapper.startIndex
            
            return sections
        }
    }
}
