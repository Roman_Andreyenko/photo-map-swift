//
//  SignUpViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/7/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class SignUpViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet private weak var emailView: UITextField!
    @IBOutlet private weak var passwordView: UITextField!
    @IBOutlet private weak var passwordConfirmationView: UITextField!
    @IBOutlet private weak var emailValidationView: UILabel!
    @IBOutlet private weak var passwordValidationView: UILabel!
    @IBOutlet private weak var passwordConfirmationValidationView: UILabel!
    @IBOutlet private weak var signUpView: UIButton!
    @IBOutlet private weak var signUpActivityIndicatorView: UIActivityIndicatorView!
    
    private weak var viewModel: SignUpViewModel!
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: SignUpViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - UI logic methods
    
    private func setupViews() {
        self.emailView.delegate = self
        self.passwordView.delegate = self
        self.passwordConfirmationView.delegate = self
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bindEmailView()
        self.bindPasswordView()
        self.bindPasswordConfirmationView()
        self.bindSignUpView()
        self.bindSignUpAction()
    }
    
    private func bindEmailView() {
        self.emailView.rx
            .text
            .asObservable()
            .bindTo(self.viewModel.email)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPasswordView() {
        self.passwordView.rx
            .text
            .asObservable()
            .bindTo(self.viewModel.password)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPasswordConfirmationView() {
        self.passwordConfirmationView.rx
            .text
            .asObservable()
            .bindTo(self.viewModel.passwordConfirmation)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindSignUpView() {
        self.signUpView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { (event) in
                self.viewModel.signUpAction.execute()
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .signUpAction
            .enabled
            .bindTo(self.signUpView.rx.isEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindSignUpAction() {
        self.viewModel
            .signUpAction
            .elements
            .subscribe(onNext: { (user) in
                AppDelegate.shared().setupRootViewController(animated: true)
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .signUpAction
            .errors
            .subscribe(onNext: { (actionError) in
                self.presentAlertWithActionError(actionError)
            })
            .addDisposableTo(self.disposeBag)
        let executingObservable = self.viewModel
            .signUpAction
            .executing.map { isExecuting in
                return !isExecuting
            }
        executingObservable.bindTo(self.signUpActivityIndicatorView.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        executingObservable.bindTo(self.view.rx.isUserInteractionEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func setupValidationView(_ validationView: UILabel!, _ validationObservable: Observable<String?>!, _ observedView: UITextField!, _ focusedView: UITextField!) {
        if (focusedView == observedView) {
            validationObservable
                .bindTo(validationView.rx.text)
                .addDisposableTo(self.disposeBag)
            observedView.delegate = nil
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.setupValidationView(self.emailValidationView, self.viewModel.validEmailObservable, self.emailView, textField)
        self.setupValidationView(self.passwordValidationView, self.viewModel.validPasswordObservable, self.passwordView, textField)
        self.setupValidationView(self.passwordConfirmationValidationView, self.viewModel.validPasswordConfirmationObservable, self.passwordConfirmationView, textField)
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
