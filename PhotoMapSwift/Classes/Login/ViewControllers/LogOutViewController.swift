//
//  LogOutViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

class LogOutViewController: BaseViewController {

    @IBOutlet private weak var logOutView: UIButton!
    @IBOutlet private weak var logOutActivityIndicatorView: UIActivityIndicatorView!
    
    private weak var viewModel: LogOutViewModel!
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: LogOutViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bindLogOutView()
        self.bindLogOutAction()
    }

    private func bindLogOutView() {
        self.logOutView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { (event) in
                self.viewModel.logOutAction.execute()
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .logOutAction
            .enabled
            .bindTo(self.logOutView.rx.isEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindLogOutAction() {
        self.viewModel
            .logOutAction
            .elements
            .subscribe(onNext: { (user) in
                AppDelegate.shared().setupRootViewController(animated: true)
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .logOutAction
            .errors
            .subscribe(onNext: { (actionError) in
                self.presentAlertWithActionError(actionError)
            })
            .addDisposableTo(self.disposeBag)
        let executingObservable = self.viewModel
            .logOutAction
            .executing.map { isExecuting in
                return !isExecuting
            }
        executingObservable.bindTo(self.logOutActivityIndicatorView.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        executingObservable.bindTo(self.view.rx.isUserInteractionEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
