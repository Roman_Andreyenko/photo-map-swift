//
//  LogInViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/7/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import RxSwift
import FirebaseAuth

class LogInViewController: BaseViewController, UITextFieldDelegate {

    @IBOutlet private weak var logInView: UITextField!
    @IBOutlet private weak var passwordView: UITextField!
    @IBOutlet private weak var logInValidationView: UILabel!
    @IBOutlet private weak var passwordValidationView: UILabel!
    @IBOutlet private weak var logInActionView: UIButton!
    @IBOutlet private weak var logInActivityIndicatorView: UIActivityIndicatorView!
    
    private weak var viewModel: LogInViewModel!
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: LogInViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI logic methods
    
    private func setupViews() {
        self.logInView.delegate = self
        self.passwordView.delegate = self
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bindLogInView()
        self.bindPasswordView()
        self.bindLogInActionView()
        self.bindLogInAction()
    }
    
    private func bindLogInView() {
        self.logInView.rx
            .text
            .asObservable()
            .bindTo(self.viewModel.login)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPasswordView() {
        self.passwordView.rx
            .text
            .asObservable()
            .bindTo(self.viewModel.password)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindLogInActionView() {
        self.logInActionView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { (event) in
                self.viewModel.logInAction.execute()
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .logInAction
            .enabled
            .bindTo(self.logInActionView.rx.isEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindLogInAction() {
        self.viewModel
            .logInAction
            .elements
            .subscribe(onNext: { (user) in
                AppDelegate.shared().setupRootViewController(animated: true)
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .logInAction
            .errors
            .subscribe(onNext: { (actionError) in
                self.presentAlertWithActionError(actionError)
            })
            .addDisposableTo(self.disposeBag)
        let executingObservable = self.viewModel
            .logInAction
            .executing.map { isExecuting in
                return !isExecuting
            }
        executingObservable.bindTo(self.logInActivityIndicatorView.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        executingObservable.bindTo(self.view.rx.isUserInteractionEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func setupValidationView(_ validationView: UILabel!, _ validationObservable: Observable<String?>!, _ observedView: UITextField!, _ focusedView: UITextField!) {
        if (focusedView == observedView) {
            validationObservable
                .bindTo(validationView.rx.text)
                .addDisposableTo(self.disposeBag)
            observedView.delegate = nil
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        self.setupValidationView(self.logInValidationView, self.viewModel.validLoginObservable, self.logInView, textField)
        self.setupValidationView(self.passwordValidationView, self.viewModel.validPasswordObservable, self.passwordView, textField)
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
