//
//  WelcomeViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/7/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class WelcomeViewController: BaseViewController {

    @IBOutlet private weak var signUpView: UIButton!
    @IBOutlet private weak var logInView: UIButton!
    
    private weak var viewModel: WelcomeViewModel!
    private var signUpViewModel : SignUpViewModel?
    private var logInViewModel : LogInViewModel?
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: WelcomeViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI logic methods
    
    private func openSignUpScreen() {
        self.signUpViewModel = SignUpViewModel()
        let signUpViewController = SignUpViewController(nibName: SignUpViewController.className, bundle: nil, viewModel: self.signUpViewModel!)
        self.navigationController?.pushViewController(signUpViewController, animated: true)
    }
    
    private func openLogInScreen() {
        self.logInViewModel = LogInViewModel()
        let logInViewController = LogInViewController(nibName: LogInViewController.className, bundle: nil, viewModel: self.logInViewModel!)
        self.navigationController?.pushViewController(logInViewController, animated: true)
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.viewModel.logInObservable = logInView.rx
            .tap
            .asObservable()
        self.viewModel.signUpObservable = signUpView.rx
            .tap
            .asObservable()
        
        self.viewModel
            .signUpObservable?
            .subscribe(onNext: { event in
                self.openSignUpScreen()
            })
            .addDisposableTo(self.disposeBag)
        
        self.viewModel
            .logInObservable?
            .subscribe(onNext: { event in
                self.openLogInScreen()
            })
            .addDisposableTo(self.disposeBag)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
