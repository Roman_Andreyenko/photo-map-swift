//
//  WelcomeViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/7/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift

class WelcomeViewModel: NSObject {
    
    public var signUpObservable: Observable<Void>? = nil
    public var logInObservable: Observable<Void>? = nil
}
