//
//  LogInViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import Action
import FirebaseAuth

class LogInViewModel: NSObject {
    
    var login = Variable<String?>("")
    var password = Variable<String?>("")
    
    private var loginRegex: NSRegularExpression!
    private var passwordRegex: NSRegularExpression!
    
    var validLoginObservable: Observable<String?>!
    var validPasswordObservable: Observable<String?>!
    var logInAction: Action<Void, FIRUser?>!
    
    override init() {
        super.init()
        
        self.initialize()
    }
    
    private func initialize() {
        self.loginRegex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        self.passwordRegex = try! NSRegularExpression(pattern: "^.{6,20}$")
        
        self.validLoginObservable = self.login.asObservable().map({ (loginText: String?) -> String? in
            if let value = loginText {
                let numberOfMatches = self.loginRegex.numberOfMatches(in: value, options: [], range: NSRange(location: 0, length: value.characters.count))
                return numberOfMatches == 0 ? NSLocalizedString("ValidEmail", comment: "") : nil
            } else {
                return NSLocalizedString("ValidEmail", comment: "")
            }
        })
        self.validPasswordObservable = self.password.asObservable().map({ (passwordText: String?) -> String? in
            if let value = passwordText {
                let numberOfMatches = self.passwordRegex.numberOfMatches(in: value, options: [], range: NSRange(location: 0, length: value.characters.count))
                return numberOfMatches == 0 ? NSLocalizedString("ValidPasswordLength", comment: "") : nil
            } else {
                return NSLocalizedString("ValidPasswordLength", comment: "")
            }
        })
        let logInActiveObservable = Observable.combineLatest(self.validLoginObservable, self.validPasswordObservable, AppDelegate.shared().reachableObservable(), resultSelector: { (validLogin: String?, validPassword: String?, isReachable: Bool) -> Bool in
            return validLogin == nil && validPassword == nil && isReachable
        })
        
        self.logInAction = Action<Void, FIRUser?>(enabledIf: logInActiveObservable, workFactory: { () -> Observable<FIRUser?> in
            return Observable<FIRUser?>.create { observer in
                FIRAuth.auth()?.signIn(withEmail: self.login.value!, password: self.password.value!) { (user, error) in
                    if let anError = error {
                        observer.onError(anError)
                    } else {
                        observer.onNext(user)
                        observer.onCompleted()
                    }
                }
                return Disposables.create()
            }
        })
    }
}
