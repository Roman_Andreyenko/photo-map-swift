//
//  LogOutViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//
import RxCocoa
import RxSwift
import Action
import FirebaseAuth

class LogOutViewModel: NSObject {
    
    var logOutAction: Action<Void, Void>!

    override init() {
        super.init()
        
        self.initialize()
    }
    
    private func initialize() {
        let logOutActiveObservable = AppDelegate.shared().reachableObservable()
        self.logOutAction = Action<Void, Void>(enabledIf: logOutActiveObservable, workFactory: { (input) -> Observable<Void> in
            return Observable<Void>.create { observer in
                do {
                    try FIRAuth.auth()?.signOut()
                    observer.onNext()
                    observer.onCompleted()
                } catch let signOutError {
                    observer.onError(signOutError)
                }
                return Disposables.create()
            }
        })
    }
}
