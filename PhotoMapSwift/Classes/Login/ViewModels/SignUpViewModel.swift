//
//  SignUpViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import Action
import FirebaseAuth

class SignUpViewModel: NSObject {

    var email = Variable<String?>("")
    var password = Variable<String?>("")
    var passwordConfirmation = Variable<String?>("")
    
    private var emailRegex: NSRegularExpression!
    private var passwordRegex: NSRegularExpression!
    
    var validEmailObservable: Observable<String?>!
    var validPasswordObservable: Observable<String?>!
    var validPasswordConfirmationObservable: Observable<String?>!
    var signUpAction: Action<Void, FIRUser?>!
    
    override init() {
        super.init()
        
        self.initialize()
    }
    
    private func initialize() {
        
        self.emailRegex = try! NSRegularExpression(pattern: "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}")
        self.passwordRegex = try! NSRegularExpression(pattern: "^.{6,20}$")
        
        self.validEmailObservable = self.email.asObservable().map({ (emailText: String?) -> String? in
            if let value = emailText {
                let numberOfMatches = self.emailRegex.numberOfMatches(in: value, options: [], range: NSRange(location: 0, length: value.characters.count))
                return numberOfMatches == 0 ? NSLocalizedString("ValidEmail", comment: "") : nil
            } else {
                return NSLocalizedString("ValidEmail", comment: "")
            }
        })
        let validPasswordDataObservable = Observable.combineLatest(self.password.asObservable(), self.passwordConfirmation.asObservable()) { passwordText, passwordConfirmationText -> (String?, String?) in
            return (self.validatePassword(passwordText: passwordText, passwordConfirmationText: passwordConfirmationText),
                    self.validatePasswordConfirmation(passwordText: passwordText, passwordConfirmationText: passwordConfirmationText))
        }
        self.validPasswordObservable = validPasswordDataObservable.map({ (validationData: (passwordValidation: String?, passwordConfirmationValidation: String?)) -> String? in
            return validationData.passwordValidation
        })
        self.validPasswordConfirmationObservable = validPasswordDataObservable.map({ (validationData: (passwordValidation: String?, passwordConfirmationValidation: String?)) -> String? in
            return validationData.passwordConfirmationValidation
        })
        let signUpActiveObservable = Observable.combineLatest(self.validEmailObservable, self.validPasswordObservable, self.validPasswordConfirmationObservable, AppDelegate.shared().reachableObservable(), resultSelector: { (validEmail: String?, validPassword: String?, validPasswordConfirmation: String?, isReachable: Bool) -> Bool in
            return validEmail == nil && validPassword == nil && validPasswordConfirmation == nil && isReachable
        })
        
        self.signUpAction = Action<Void, FIRUser?>(enabledIf: signUpActiveObservable, workFactory: { () -> Observable<FIRUser?> in
            return Observable<FIRUser?>.create { observer in
                FIRAuth.auth()?.createUser(withEmail: self.email.value!, password: self.password.value!) { (user, error) in
                    if let anError = error {
                        observer.onError(anError)
                    } else {
                        observer.onNext(user)
                        observer.onCompleted()
                    }
                }
                return Disposables.create()
            }
        })
    }
    
    private func validatePassword(passwordText: String?, passwordConfirmationText: String?) -> String? {
        if let value = passwordText {
            let numberOfMatches = self.passwordRegex.numberOfMatches(in: value, options: [], range: NSRange(location: 0, length: value.characters.count))
            if numberOfMatches == 0 {
                return NSLocalizedString("ValidPasswordLength", comment: "")
            } else {
                return passwordConfirmationText == value
                    ? nil : NSLocalizedString("ValidPassword", comment: "")
            }
        } else {
            return NSLocalizedString("ValidPasswordLength", comment: "")
        }
    }
    
    private func validatePasswordConfirmation(passwordText: String?, passwordConfirmationText: String?) -> String? {
        if let value = passwordConfirmationText {
            return passwordText != value ? NSLocalizedString("ValidConfirmPassword", comment: "") : nil
        } else {
            return NSLocalizedString("ValidConfirmPassword", comment: "")
        }
    }
}
