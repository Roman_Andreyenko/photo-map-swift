//
//  MKMapView+Rx.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/24/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import MapKit

extension Reactive where Base: MKMapView {
    
    public var delegateProxy: DelegateProxy {
        return RxMKMapViewDelegateProxy.proxyForObject(self.base)
    }
    
    public var didSelectAnnotationView: Observable<(mapView: MKMapView, annotationView: MKAnnotationView)> {
        return self.delegateProxy.sentMessage(#selector(MKMapViewDelegate.mapView(_:didSelect:))).map({ objects -> (mapView: MKMapView, annotationView: MKAnnotationView) in
            let mapView = objects[0] as! MKMapView
            let annotationView = objects[1] as! MKAnnotationView
            return (mapView: mapView, annotationView: annotationView)
        })
    }
    
    public var didDeselectAnnotationView: Observable<(mapView: MKMapView, annotationView: MKAnnotationView)> {
        return self.delegateProxy.sentMessage(#selector(MKMapViewDelegate.mapView(_:didDeselect:))).map({ objects -> (mapView: MKMapView, annotationView: MKAnnotationView) in
            let mapView = objects[0] as! MKMapView
            let annotationView = objects[1] as! MKAnnotationView
            return (mapView: mapView, annotationView: annotationView)
        })
    }
}
