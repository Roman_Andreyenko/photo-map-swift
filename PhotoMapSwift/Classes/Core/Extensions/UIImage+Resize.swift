//
//  UIImage+Resize.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/21/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension UIImage {
    
    public func resizeImageWithMaxSizeLength(_ maxSizeLength: CGFloat) -> UIImage? {
        let scaleFactor = (self.size.width > self.size.height) ? maxSizeLength / self.size.width : maxSizeLength / self.size.height;
        let newWidth = CGFloat(truncf(Float(self.size.width * scaleFactor)))
        let newHeight = CGFloat(truncf(Float(self.size.height * scaleFactor)))
        let newSize = CGSize(width: newWidth, height: newHeight)
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0)
        self.draw(in: CGRect(x: 0.0, y: 0.0, width: newSize.width, height: newSize.height))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext();
        return newImage
    }
}
