//
//  UIColor+AppColors.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 4/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension UIColor {

    public class func natureCategoryColor() -> UIColor {
        return UIColor(colorLiteralRed: 87.0 / 255.0, green: 142.0 / 255.0, blue: 24.0 / 255.0, alpha: 1.0)
    }
    
    public class func friendsCategoryColor() -> UIColor {
        return UIColor(colorLiteralRed: 244.0 / 255.0, green: 165.0 / 255.0, blue: 35.0 / 255.0, alpha: 1.0)
    }
    
    public class func defaultCategoryColor() -> UIColor {
        return UIColor(colorLiteralRed: 54.0 / 255.0, green: 142.0 / 255.0, blue: 223.0 / 255.0, alpha: 1.0)
    }
    
}
