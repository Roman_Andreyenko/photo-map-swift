//
//  UIPickerView+Rx.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/27/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base: UIPickerView {

    public var delegateProxy: DelegateProxy {
        return RxUIPickerViewDelegateProxy.proxyForObject(self.base)
    }
    
    public var didSelectRowInComponent: Observable<(pickerView: UIPickerView, row: Int, component: Int)> {
        return self.delegateProxy.sentMessage(#selector(UIPickerViewDelegate.pickerView(_:didSelectRow:inComponent:))).map({ objects -> (pickerView: UIPickerView, row: Int, component: Int) in
            let pickerView = objects[0] as! UIPickerView
            let row = objects[1] as! Int
            let component = objects[2] as! Int
            return (pickerView: pickerView, row: row, component: component)
        })
    }
}
