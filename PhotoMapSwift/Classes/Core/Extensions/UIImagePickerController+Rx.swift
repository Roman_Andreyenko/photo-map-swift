//
//  UIImagePickerController+Rx.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/27/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base: UIImagePickerController {

    public var delegateProxy: DelegateProxy {
        return RxUIImagePickerControllerDelegateProxy.proxyForObject(self.base)
    }
    
    public var didFinishPickingMediaWithInfo: Observable<(picker: UIImagePickerController, info: [String : Any])> {
        return self.delegateProxy.sentMessage(#selector(UIImagePickerControllerDelegate.imagePickerController(_:didFinishPickingMediaWithInfo:))).map({ objects -> (picker: UIImagePickerController, info: [String : Any]) in
            let picker = objects[0] as! UIImagePickerController
            let info = objects[1] as! [String : Any]
            return (picker: picker, info: info)
        })
    }
}
