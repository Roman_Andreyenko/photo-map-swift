//
//  UISearchBar+Rx.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base: UISearchBar {

    public var delegateProxy: DelegateProxy {
        return RxUISearchBarDelegateProxy.proxyForObject(self.base)
    }
    
    public var textDidChange: Observable<(searchBar: UISearchBar, searchText: String)> {
        return self.delegateProxy.sentMessage(#selector(UISearchBarDelegate.searchBar(_:textDidChange:))).map({ objects -> (searchBar: UISearchBar, searchText: String) in
            let searchBar = objects[0] as! UISearchBar
            let searchText = objects[1] as! String
            return (searchBar: searchBar, searchText: searchText)
        })
    }

    public var searchBarSearchButtonClicked: Observable<UISearchBar> {
        return self.delegateProxy.sentMessage(#selector(UISearchBarDelegate.searchBarSearchButtonClicked(_:))).map({ objects -> UISearchBar in
            let searchBar = objects[0] as! UISearchBar
            return searchBar
        })
    }
}
