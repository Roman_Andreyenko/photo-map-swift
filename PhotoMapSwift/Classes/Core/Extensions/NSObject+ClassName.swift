//
//  NSObject+ClassName.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/9/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Foundation

extension NSObject {
    
    var className: String {
        return String(describing: type(of: self))
    }
    
    class var className: String {
        return String(describing: self)
    }
}
