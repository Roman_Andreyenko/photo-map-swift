//
//  UITableView+Rx.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import UIKit

extension Reactive where Base: UITableView {

    public var delegateProxy: DelegateProxy {
        return RxUITableViewDelegateProxy.proxyForObject(self.base)
    }
    
    public var willDisplayCellForRowAtIndexPath: Observable<(tableView: UITableView, cell: UITableViewCell, indexPath: IndexPath)> {
        return self.delegateProxy.sentMessage(#selector(UITableViewDelegate.tableView(_:willDisplay:forRowAt:))).map({ objects -> (tableView: UITableView, cell: UITableViewCell, indexPath: IndexPath) in
            let tableView = objects[0] as! UITableView
            let cell = objects[1] as! UITableViewCell
            let indexPath = objects[2] as! IndexPath
            return (tableView: tableView, cell: cell, indexPath: indexPath)
        })
    }
    
    public var didSelectRowAtIndexPath: Observable<(tableView: UITableView, indexPath: IndexPath)> {
        return self.delegateProxy.sentMessage(#selector(UITableViewDelegate.tableView(_:didSelectRowAt:))).map({ objects -> (tableView: UITableView, indexPath: IndexPath) in
            let tableView = objects[0] as! UITableView
            let indexPath = objects[1] as! IndexPath
            return (tableView: tableView, indexPath: indexPath)
        })
    }
}
