//
//  UIViewController+Transitions.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/14/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func presentAlertWithMessage(_ message: String) {
        self.presentAlertWithTitle(NSLocalizedString("Warning", comment: ""), andMessage: message)
    }
    
    public func presentAlertWithTitle(_ title: String, andMessage message: String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("OK", comment:""), style: .default)
        alertController.addAction(defaultAction)
        self.present(alertController, animated: true)
    }
    
    public func presentHorizontally(viewController: UIViewController, animated: Bool, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromRight
        self.view.window?.layer.add(transition, forKey: nil)
        self.present(viewController, animated: animated, completion: completion)
    }
    
    public func dismissHorizontallyViewController(animated: Bool, completion: (() -> Swift.Void)? = nil) {
        let transition = CATransition()
        transition.duration = 0.5
        transition.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionEaseInEaseOut)
        transition.type = kCATransitionPush
        transition.subtype = kCATransitionFromLeft
        self.view.window?.layer.add(transition, forKey: nil)
        self.dismiss(animated: animated, completion: completion)
    }
    
    public func presentSettingsAlertWith(message: String) {
        let alertController = UIAlertController(title: NSLocalizedString("Warning", comment: ""), message:message, preferredStyle: .alert)
        let defaultAction = UIAlertAction(title: NSLocalizedString("OK", comment:""), style: .default)
        alertController.addAction(defaultAction)
        let settingsAction = UIAlertAction(title: NSLocalizedString("Settings", comment:""), style: .default, handler: { action in
            UIApplication.shared.open(URL(string: UIApplicationOpenSettingsURLString)!, options: [:])
        })
        alertController.addAction(settingsAction)
        self.present(alertController, animated: true)
    }
}
