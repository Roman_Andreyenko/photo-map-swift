//
//  NSError+CustomDescription.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

extension NSError {
    
    public class func createError(withLocalizedDescription localizedDescription: String) -> NSError {
        let userInfo: [AnyHashable : Any] = [
            NSLocalizedDescriptionKey : localizedDescription,
            NSLocalizedFailureReasonErrorKey : localizedDescription]
        return NSError(domain: "UserDataErrorDomain", code: 0, userInfo: userInfo)
    }
}
