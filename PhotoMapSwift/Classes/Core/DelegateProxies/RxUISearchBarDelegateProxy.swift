//
//  RxUISearchBarDelegateProxy.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/29/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

class RxUISearchBarDelegateProxy: DelegateProxy, UISearchBarDelegate, DelegateProxyType {

    static func currentDelegateFor(_ object: AnyObject) -> AnyObject? {
        let searchBar = object as! UISearchBar
        return searchBar.delegate
    }
    
    static func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {
        let searchBar = object as! UISearchBar
        searchBar.delegate = delegate as? UISearchBarDelegate
    }
}
