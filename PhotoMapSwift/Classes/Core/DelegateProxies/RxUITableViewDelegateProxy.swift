//
//  RxUITableViewDelegateProxy.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/31/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

class RxUITableViewDelegateProxy: DelegateProxy, UITableViewDelegate, DelegateProxyType {

    static func currentDelegateFor(_ object: AnyObject) -> AnyObject? {
        let tableView = object as! UITableView
        return tableView.delegate
    }
    
    static func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {
        let tableView = object as! UITableView
        tableView.delegate = delegate as? UITableViewDelegate
    }
    
}
