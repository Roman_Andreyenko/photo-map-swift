//
//  RxUIImagePickerControllerDelegateProxy.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/27/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

class RxUIImagePickerControllerDelegateProxy: DelegateProxy, UIImagePickerControllerDelegate, UINavigationControllerDelegate, DelegateProxyType {

    static func currentDelegateFor(_ object: AnyObject) -> AnyObject? {
        let pickerController = object as! UIImagePickerController
        return pickerController.delegate
    }
    
    static func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {
        let pickerController = object as! UIImagePickerController
        pickerController.delegate = delegate as? UIImagePickerControllerDelegate & UINavigationControllerDelegate
    }
    
}
