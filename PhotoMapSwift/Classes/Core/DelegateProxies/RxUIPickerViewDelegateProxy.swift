//
//  RxUIPickerViewDelegateProxy.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/27/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift
import RxCocoa
import UIKit

class RxUIPickerViewDelegateProxy: DelegateProxy, UIPickerViewDelegate, DelegateProxyType {

    static func currentDelegateFor(_ object: AnyObject) -> AnyObject? {
        let pickerView = object as! UIPickerView
        return pickerView.delegate
    }
    
    static func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {
        let pickerView = object as! UIPickerView
        pickerView.delegate = delegate as? UIPickerViewDelegate
    }
}
