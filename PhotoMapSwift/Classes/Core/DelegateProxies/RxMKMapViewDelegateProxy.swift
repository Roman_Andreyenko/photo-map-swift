//
//  RxMKMapViewDelegateProxy.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/24/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift
import RxCocoa
import MapKit

class RxMKMapViewDelegateProxy: DelegateProxy, MKMapViewDelegate, DelegateProxyType {
    
    static func currentDelegateFor(_ object: AnyObject) -> AnyObject? {
        let mapView = object as! MKMapView
        return mapView.delegate
    }

    static func setCurrentDelegate(_ delegate: AnyObject?, toObject object: AnyObject) {
        let mapView = object as! MKMapView
        mapView.delegate = delegate as? MKMapViewDelegate
    }
}
