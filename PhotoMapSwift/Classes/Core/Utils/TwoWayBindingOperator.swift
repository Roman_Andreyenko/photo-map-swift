//
//  TwoWayBindingOperator.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 4/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxSwift
import RxCocoa

infix operator <-> : DefaultPrecedence

func <-> <T>(property: ControlProperty<T>, variable: Variable<T>) -> Disposable {
//    if T.self == String.self {
//        #if DEBUG
//            "It is ok to delete this message, but this is here to warn that you are maybe trying to bind to some `rx.text` property directly to variable.\n" +
//                "That will usually work ok, but for some languages that use IME, that simplistic method could cause unexpected issues because it will return intermediate results while text is being inputed.\n" +
//                "REMEDY: Just use `textField <-> variable` instead of `textField.rx.text <-> variable`.\n" +
//                "Find out more here: https://github.com/ReactiveX/RxSwift/issues/649\n"
//        #endif
//    }
    
    let bindToUIDisposable = variable.asObservable()
        .bindTo(property)
    let bindToVariable = property
        .subscribe(onNext: { n in
            variable.value = n
        }, onCompleted:  {
            bindToUIDisposable.dispose()
        })
    
    return Disposables.create(bindToUIDisposable, bindToVariable)
}
