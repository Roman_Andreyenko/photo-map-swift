//
//  PhotoIndex.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/28/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import FirebaseDatabase

class PhotoIndex: NSObject {

    private static let PhotoKeyKey = "photoKey"
    private static let HashtagKeyKey = "hashtagKey"
    
    var key: String?
    var photoKey: String
    var hashtagKey: String
    
    public class func getReference() -> FIRDatabaseReference {
        return FIRDatabase.database().reference().child(PhotoIndex.className)
    }
    
    init(photoKey: String, hashtagKey: String) {
        self.key = nil
        self.photoKey = photoKey
        self.hashtagKey = hashtagKey
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.key = snapshot.key
        self.photoKey = snapshotValue[PhotoIndex.PhotoKeyKey] as! String
        self.hashtagKey = snapshotValue[PhotoIndex.HashtagKeyKey] as! String
    }
    
    public func toAnyObject() -> Any {
        return [
            PhotoIndex.PhotoKeyKey: self.photoKey,
            PhotoIndex.HashtagKeyKey: self.hashtagKey
        ]
    }
    
    public class func create(withPhotoKey photoKey: String, withHashtagKey hashtagKey: String) -> Observable<PhotoIndex?> {
        let photoIndex = PhotoIndex(photoKey: photoKey, hashtagKey: hashtagKey)
        return Observable<PhotoIndex?>.create { observer -> Disposable in
            PhotoIndex.getReference().childByAutoId().setValue(photoIndex.toAnyObject()) { error, reference in
                if let error = error {
                    observer.onError(error)
                } else {
                    photoIndex.key = reference.key
                    observer.onNext(photoIndex)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        }
    }
    
    public class func readAll() -> Observable<[PhotoIndex]> {
        return Observable<[PhotoIndex]>.create { observer -> Disposable in
            PhotoIndex.getReference().observeSingleEvent(of: .value, with: { (snapshot) in
                var photoIndexes = [PhotoIndex]()
                if snapshot.exists() {
                    photoIndexes.reserveCapacity(Int(snapshot.childrenCount))
                    for child in snapshot.children {
                        let photoIndex = PhotoIndex(snapshot: child as! FIRDataSnapshot)
                        photoIndexes.append(photoIndex)
                    }
                }
                observer.onNext(photoIndexes)
                observer.onCompleted()
            }, withCancel: { error in
                observer.onError(error)
            })
            return Disposables.create()
        }
    }
}
