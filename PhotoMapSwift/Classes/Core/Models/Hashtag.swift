//
//  Hashtag.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/27/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import FirebaseDatabase

class Hashtag: NSObject {

    private static let HashtagKey = "hashtag"
    
    var key: String?
    var hashtag: String
    
    public class func parseTagsInText(_ textWithTags: String) -> [String] {
     
        let regex = try! NSRegularExpression(pattern: "#(\\w+)")
        let matches = regex.matches(in: textWithTags, options: [], range: NSRange(location: 0, length: textWithTags.characters.count))
        var hashtags = Set<String>(minimumCapacity: matches.count)
        for match in matches {
            let matchRange = match.rangeAt(1)
            let wordRange = textWithTags.index(textWithTags.startIndex, offsetBy: matchRange.location)..<textWithTags.index(textWithTags.startIndex, offsetBy: matchRange.location + matchRange.length)
            let word = textWithTags.substring(with: wordRange)
            hashtags.insert(word)
        }
        
        return Array(hashtags)
    }

    public class func findHashtag(_ hashtags: [Hashtag], withTag tag: String) -> Hashtag? {
        for hashtag in hashtags {
            if hashtag.hashtag == tag {
                return hashtag
            }
        }
        
        return nil
    }
    
    public class func getReference() -> FIRDatabaseReference {
        return FIRDatabase.database().reference().child(Hashtag.className)
    }
    
    init(hashtag: String) {
        self.key = nil
        self.hashtag = hashtag
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.key = snapshot.key
        self.hashtag = snapshotValue[Hashtag.HashtagKey] as! String
    }
    
    public func toAnyObject() -> Any {
        return [
            Hashtag.HashtagKey: self.hashtag
        ]
    }
    
    public class func readAll() -> Observable<[Hashtag]> {
        return Observable<[Hashtag]>.create { observer -> Disposable in
            Hashtag.getReference().observeSingleEvent(of: .value, with: { (snapshot) in
                var hashtags = [Hashtag]()
                if snapshot.exists() {
                    hashtags.reserveCapacity(Int(snapshot.childrenCount))
                    for child in snapshot.children {
                        let hashtag = Hashtag(snapshot: child as! FIRDataSnapshot)
                        hashtags.append(hashtag)
                    }
                }
                observer.onNext(hashtags)
                observer.onCompleted()
            }, withCancel: { error in
                observer.onError(error)
            })
            return Disposables.create()
        }
    }
    
    public class func create(_ hashtag: String) -> Observable<Hashtag?> {
        let hashtagInstance = Hashtag(hashtag: hashtag)
        return Observable<Hashtag?>.create { observer -> Disposable in
            Hashtag.getReference().childByAutoId().setValue(hashtagInstance.toAnyObject()) { error, reference in
                if let error = error {
                    observer.onError(error)
                } else {
                    hashtagInstance.key = reference.key
                    observer.onNext(hashtagInstance)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        }
    }
}
