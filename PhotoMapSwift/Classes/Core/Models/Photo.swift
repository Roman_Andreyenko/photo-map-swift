//
//  Photo.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/21/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import Firebase
import FirebaseDatabase
import RxCocoa
import RxSwift

class Photo: NSObject {

    enum Category : String {
        case Default
        case Friends
        case Nature
    }
    
    private static let PicturesPath = "pictures"
    private static let PicturesThumbnailPath = "picturesThumbnail"
    
    private static let OwnerIdKey = "ownerId"
    private static let CreatedTimeKey = "createdTime"
    private static let CategoryKey = "category"
    private static let DescriptionTextKey = "descriptionText"
    private static let LatitudeKey = "latitude"
    private static let LongitudeKey = "longitude"
    
    var key: String?
    var ownerId: String
    var pictureData: Data?
    var pictureThumbnailData: Data?
    var createdTime: Date
    var category: Category
    var descriptionText: String
    var latitude: NSNumber
    var longitude: NSNumber
    
    class func getReference() -> FIRDatabaseReference {
        return FIRDatabase.database().reference().child(Photo.className)
    }
    
    class func getPicturesReference() -> FIRStorageReference {
        return FIRStorage.storage().reference().child(Photo.PicturesPath)
    }
    
    class func getPicturesTnumbnailReference() -> FIRStorageReference {
        return FIRStorage.storage().reference().child(Photo.PicturesThumbnailPath)
    }
    
    init(ownerId: String, pictureData: Data?, pictureThumbnailData: Data?, createdTime: Date, category: Category, descriptionText: String, latitude: NSNumber, longitude: NSNumber) {
        self.key = nil
        self.ownerId = ownerId
        self.pictureData = pictureData
        self.pictureThumbnailData = pictureThumbnailData
        self.createdTime = createdTime
        self.category = category
        self.descriptionText = descriptionText
        self.latitude = latitude
        self.longitude = longitude
    }
    
    init(snapshot: FIRDataSnapshot) {
        let snapshotValue = snapshot.value as! [String: AnyObject]
        self.key = snapshot.key
        self.ownerId = snapshotValue[Photo.OwnerIdKey] as! String
        
        self.createdTime = Date(timeIntervalSince1970: (snapshotValue[Photo.CreatedTimeKey] as! NSNumber).doubleValue)
        self.category = Category(rawValue: snapshotValue[Photo.CategoryKey] as! String)!
        self.descriptionText = snapshotValue[Photo.DescriptionTextKey] as! String
        self.latitude = snapshotValue[Photo.LatitudeKey] as! NSNumber
        self.longitude = snapshotValue[Photo.LongitudeKey] as! NSNumber
    }
    
    public func toAnyObject() -> Any {
        return [
            Photo.OwnerIdKey: self.ownerId,
            Photo.CreatedTimeKey: self.createdTime.timeIntervalSince1970,
            Photo.CategoryKey: self.category.rawValue,
            Photo.DescriptionTextKey: self.descriptionText,
            Photo.LatitudeKey: self.latitude,
            Photo.LongitudeKey: self.longitude
        ]
    }
    
    public func uploadPictureObservable() -> Observable<Photo> {
        return uploadPicture(data: self.pictureData!, baseReference: Photo.getPicturesReference())
    }
    
    public func uploadPictureThumbnailObservable() -> Observable<Photo> {
        return uploadPicture(data: self.pictureThumbnailData!, baseReference: Photo.getPicturesTnumbnailReference())
    }
    
    public func downloadPictureObservable() -> Observable<Data> {
        return downloadPicture(baseReference: Photo.getPicturesReference(), localData: self.pictureData)
    }

    public func downloadPictureThumbnailObservable() -> Observable<Data> {
        return downloadPicture(baseReference: Photo.getPicturesTnumbnailReference(), localData: self.pictureThumbnailData)
    }
    
    public class func create(ownerId: String, pictureData: Data?, pictureThumbnailData: Data?, createdTime: Date, category: Category, descriptionText: String, latitude: NSNumber, longitude: NSNumber) -> Observable<Photo?> {
        let photo = Photo(ownerId: ownerId, pictureData: pictureData, pictureThumbnailData: pictureThumbnailData, createdTime: createdTime, category: category, descriptionText: descriptionText, latitude: latitude, longitude: longitude)
        return Observable<Photo?>.create { observer -> Disposable in
            Photo.getReference().childByAutoId().setValue(photo.toAnyObject()) { error, reference in
                if let error = error {
                    observer.onError(error)
                } else {
                    photo.key = reference.key
                    observer.onNext(photo)
                    observer.onCompleted()
                }
            }
            return Disposables.create()
        }
    }
    
    public class func readByOwnerId(_ ownerId: String) -> Observable<[Photo]> {
        return Observable<[Photo]>.create { observer -> Disposable in
            Photo.getReference().queryOrdered(byChild: Photo.CreatedTimeKey).observeSingleEvent(of: .value, with: { snapshot in
                var photos = [Photo]()
                if snapshot.exists() {
                    for child in snapshot.children {
                        let childSnapshot = child as! FIRDataSnapshot
                        let snapshotValue = childSnapshot.value as! [String: AnyObject]
                        let childOwnerId = snapshotValue[Photo.OwnerIdKey] as! String
                        if childOwnerId == ownerId {
                            let photo = Photo(snapshot: childSnapshot)
                            photos.append(photo)
                        }
                    }
                }
                observer.onNext(photos)
                observer.onCompleted()
            }, withCancel: { error in
                observer.onError(error)
            })
            return Disposables.create()
        }
    }
    
    private func uploadPicture(data: Data, baseReference: FIRStorageReference) -> Observable<Photo> {
        return Observable<Photo>.create { observer -> Disposable in
            let task = baseReference.child(self.generatePictureFileName()).put(data, metadata: nil) { (metadata, error) in
                if metadata != nil {
                    observer.onNext(self)
                    observer.onCompleted()
                } else if let error = error {
                    observer.onError(error)
                } else {
                    observer.onError(NSError.createError(withLocalizedDescription: NSLocalizedString("IncorrectPhotoData", comment: "")))
                }
            }
            return Disposables.create {
                task.cancel()
            }
        }
    }
    
    private func downloadPicture(baseReference: FIRStorageReference, localData: Data?) -> Observable<Data> {
        return Observable<Data>.create { observer -> Disposable in
            var task: FIRStorageDownloadTask? = nil
            if self.key == nil {
                if let data = localData {
                    observer.onNext(data)
                    observer.onCompleted()
                } else {
                    observer.onError(NSError.createError(withLocalizedDescription: NSLocalizedString("IncorrectPhotoData", comment: "")))
                }
            } else {
                task = baseReference.child(self.generatePictureFileName())
                    .data(withMaxSize: Int64.max) { data, error in
                        if let data = data {
                            observer.onNext(data)
                            observer.onCompleted()
                        } else if let error = error {
                            observer.onError(error)
                        } else {
                            observer.onError(NSError.createError(withLocalizedDescription: NSLocalizedString("IncorrectPhotoData", comment: "")))
                        }
                }
            }
            return Disposables.create {
                if let task = task {
                    task.cancel()
                }
            }
        }
    }
    
    private func generatePictureFileName() -> String {
        return self.ownerId + "_" + String(format: "%f", self.createdTime.timeIntervalSince1970)
    }
}
