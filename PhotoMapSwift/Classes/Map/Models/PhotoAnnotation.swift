//
//  PhotoAnnotation.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/23/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import MapKit

class PhotoAnnotation: NSObject, MKAnnotation {

    let photo: Photo
    
    init(photo: Photo) {
        self.photo = photo
    }
    
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.photo.latitude.doubleValue, longitude: self.photo.longitude.doubleValue)
    }
    
    var title: String? {
        return self.photo.descriptionText.characters.count > 0 ? self.photo.descriptionText : " "
    }
    
    var subtitle: String? {
        return DateManager.sharedInstance.MM_dd_yyyy.string(from: self.photo.createdTime)
    }
}
