//
//  MapViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/20/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import MapKit
import RxCocoa
import RxSwift
import CoreLocation

class MapViewController: BaseViewController, MKMapViewDelegate, UIPickerViewDelegate, UIPickerViewDataSource, UITextViewDelegate {

    public static let PhotoAddedNotification = NSNotification.Name("PhotoAddedNotification")
    
    private static let PickerViewCellHeight: CGFloat = 56.0
    
    @IBOutlet private weak var filterCategoriesView: UIButton!
    @IBOutlet private weak var createPhotoView: UIButton!
    @IBOutlet private weak var changeNavigationModeView: UIButton!
    @IBOutlet private weak var mapView: MKMapView!
    @IBOutlet private weak var createPhotoPopupOverlayView: UIView!
    @IBOutlet private weak var createPhotoPopupView: UIView!
    @IBOutlet private weak var pickedPhotoView: UIImageView!
    @IBOutlet private weak var pickedPhotoCreatedDateView: UILabel!
    @IBOutlet private weak var selectedCategoryView: UIButton!
    @IBOutlet private weak var createPhotoDescriptionTextView: UITextView!
    @IBOutlet private weak var createPhotoDoneView: UIButton!
    @IBOutlet private weak var createPhotoCancelView: UIButton!
    @IBOutlet private weak var categoryPickerView: UIPickerView!
    @IBOutlet private weak var categoryPickerOverlay: UIView!
    @IBOutlet private weak var categoryPickerDoneView: UIButton!
    @IBOutlet private weak var mapActivityIndicatorView: UIActivityIndicatorView!
    
    private var imagePicker: UIImagePickerController!
    
    @IBOutlet private weak var mapLongPressGestureRecognizer: UILongPressGestureRecognizer!
    @IBOutlet private var selectedPinTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet private weak var pickedPhotoViewTapGestureRecognizer: UITapGestureRecognizer!
    
    private var filterViewModel: FilterCategoryViewModel!
    private var fullPhotoViewModel: FullscreenPhotoViewModel!
    private weak var viewModel: MapViewModel!

    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: MapViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.bindViewModel()
        self.viewModel.requestUserLocationAuthorization()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI logic methods
    
    private func setupViews() {
        self.imagePicker = UIImagePickerController()
        self.mapView.userTrackingMode = .follow
        self.mapView.delegate = self
        self.createPhotoDescriptionTextView.delegate = self
        self.categoryPickerView.delegate = self
        self.categoryPickerView.dataSource = self
        self.createPhotoDescriptionTextView.layer.borderColor = UIColor.lightGray.cgColor
        self.createPhotoDescriptionTextView.layer.borderWidth = 2.0
        self.createPhotoPopupView.layer.cornerRadius = 5.0
        self.setupShadowForView(self.createPhotoPopupView, withOffsetSize: CGSize(width: 3.0, height: 3.0))
        self.setupShadowForView(self.pickedPhotoView, withOffsetSize: CGSize(width: -3.0, height: 3.0))
    }
    
    private func setupShadowForView(_ view: UIView, withOffsetSize offsetSize: CGSize) {
        view.layer.masksToBounds = false
        view.layer.shadowOffset = offsetSize;
        view.layer.shadowRadius = 3.0
        view.layer.shadowOpacity = 0.5
        view.layer.shadowColor = UIColor.black.cgColor
    }
    
    private func showImagePickerWithType(_ type: UIImagePickerControllerSourceType) {
        if UIImagePickerController.isSourceTypeAvailable(type) {
            self.imagePicker.sourceType = type
            self.present(self.imagePicker, animated: true)
        }
    }
    
    private func showPhotoCreationAlert() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let takePictureAction = UIAlertAction(title: NSLocalizedString("TakePicture", comment: ""), style: .default) { alertAction in
            #if !((arch(i386) || arch(x86_64)) && os(iOS))
                self.showImagePickerWithType(.camera)
            #else
                self.presentAlertWithMessage("This feature isn't supported for iOS simulator, please, launch the app with real device")
            #endif
        }
        let chooseFromLibraryAction = UIAlertAction(title: NSLocalizedString("ChooseFromLibrary", comment: ""), style: .default) { alertAction in
            self.showImagePickerWithType(.photoLibrary)
        }
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: .cancel)
        alertController.addAction(takePictureAction)
        alertController.addAction(chooseFromLibraryAction)
        alertController.addAction(cancelAction)
        self.present(alertController, animated: true)
    }
    
    private func setupSelectedCategory(_ category: Photo.Category) {
        self.viewModel.selectedCategory = category
        self.selectedCategoryView.setTitle(self.viewModel.selectedCategory.rawValue.uppercased(), for: .normal)
        self.selectedCategoryView.setImage(PhotoPinAnnotationView.imageIconForCategory(self.viewModel.selectedCategory), for: .normal)
    }
    
    private func presentFullPhotoScreen(withPhoto photo: Photo) {
        self.fullPhotoViewModel = FullscreenPhotoViewModel(photo: photo)
        let fullscreenPhotoViewController = FullscreenPhotoViewController(nibName: FullscreenPhotoViewController.className, bundle: nil, viewModel: self.fullPhotoViewModel)
        self.presentHorizontally(viewController: fullscreenPhotoViewController, animated: false)
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bindActivityExecuting()
        self.bindPhotoLoading()
        self.bindPhotoLocationViewModel()
        self.bindPhotoCreationViewModel()
        self.bindMapNavigationModeViewModel()
        self.bindFilteredCategoriesViewModel()
        self.bindPinPhotoLoadingViewModel()
        self.bindSelectedPhotoCategoryViewModel()
        self.bindFullPhotoAppearing()
        self.bindKeyboardAppearing()
    }

    private func bindPhotoLoading() {
        AppDelegate.shared()
            .reachableObservable()
            .subscribe(onNext: { isReachable in
                if isReachable {
                    self.viewModel.loadPhotosAction.execute()
                } else {
                    self.presentSettingsAlertWith(message: NSLocalizedString("NoInternetConnection", comment: ""))
                }
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindMapNavigationModeViewModel() {
        self.mapView
            .rx
            .observeWeakly(MKUserTrackingMode.self, #keyPath(MKMapView.userTrackingMode))
            .map { userTrackingMode in
                return userTrackingMode == .follow
            }
            .bindTo(self.changeNavigationModeView.rx.isSelected)
            .addDisposableTo(self.disposeBag)
        self.changeNavigationModeView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { event in
                self.mapView.userTrackingMode = self.changeNavigationModeView.isSelected ? .none : .follow
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPhotoLocationViewModel() {
        let mapLongPressRecognisionObservable = self.mapLongPressGestureRecognizer
            .rx
            .event
            .asObservable()
            .filter { longPressGestureRecognizer in
                return longPressGestureRecognizer.state == .ended
            }
            .map { longPressGestureRecognizer -> CLLocation in
                let touchPoint = longPressGestureRecognizer.location(in: self.mapView)
                let coordinate = self.mapView.convert(touchPoint, toCoordinateFrom: self.mapView)
                return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        let createPhotoButtonObservable = self.createPhotoView
            .rx
            .tap
            .asObservable()
            .map { sender -> CLLocation in
                let coordinate = self.mapView.userLocation.coordinate
                return CLLocation(latitude: coordinate.latitude, longitude: coordinate.longitude)
        }
        Observable<CLLocation>.merge([mapLongPressRecognisionObservable, createPhotoButtonObservable])
            .bindTo(self.viewModel.photoLocation)
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .photoLocation
            .asObservable()
            .filter({ location -> Bool in
                return location != nil
            })
            .subscribe(onNext: { photoLocation in
                self.showPhotoCreationAlert()
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindFullPhotoAppearing() {
        self.pickedPhotoViewTapGestureRecognizer
            .rx
            .event
            .asObservable()
            .subscribe(onNext: { gestureRecognizer in
                self.presentFullPhotoScreen(withPhoto: self.viewModel.getCurrentPhoto())
            })
            .addDisposableTo(self.disposeBag)
        self.selectedPinTapGestureRecognizer
            .rx
            .event
            .asObservable()
            .subscribe(onNext: { gestureRecognizer in
                let pinAnnotationView = gestureRecognizer.view as! PhotoPinAnnotationView
                let photoAnnotation = pinAnnotationView.annotation as! PhotoAnnotation
                self.presentFullPhotoScreen(withPhoto: photoAnnotation.photo)
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPhotoCreationViewModel() {
        self.imagePicker
            .rx
            .didFinishPickingMediaWithInfo
            .subscribe(onNext: { (picker: UIImagePickerController, info: [String : Any]) in
                let image = info[UIImagePickerControllerOriginalImage] as! UIImage
                self.viewModel.setPhotoDataWith(originalImage: image)
                self.viewModel.photoCreatedDate = Date()
                self.createPhotoPopupOverlayView.isHidden = false
                self.pickedPhotoView.image = image;
                self.pickedPhotoCreatedDateView.text = self.viewModel.formatPhotoCreatedDate()
                self.createPhotoDescriptionTextView.text = ""
                self.categoryPickerView.selectRow(0, inComponent: 0, animated: false)
                self.setupSelectedCategory(self.viewModel.categories[0])
                picker.dismiss(animated: true)
            })
            .addDisposableTo(self.disposeBag)
        self.createPhotoDescriptionTextView
            .rx
            .text
            .orEmpty
            .bindTo(self.viewModel.photoDescriptionText)
            .addDisposableTo(self.disposeBag)
        self.createPhotoCancelView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { event in
                self.createPhotoDescriptionTextView.resignFirstResponder()
                self.createPhotoPopupOverlayView.isHidden = true
                self.viewModel.clearPhotoData()
            })
            .addDisposableTo(self.disposeBag)
        self.createPhotoDoneView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { sender in
                self.viewModel.createPhotoAction.execute()
            })
            .addDisposableTo(self.disposeBag)
        AppDelegate.shared()
            .reachableObservable()
            .bindTo(self.createPhotoDoneView.rx.isEnabled)
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .createPhotoAction
            .elements
            .subscribe(onNext: { photoAnnotation in
                self.createPhotoDescriptionTextView.resignFirstResponder()
                self.createPhotoPopupOverlayView.isHidden = true
                self.mapView.addAnnotation(photoAnnotation!)
                NotificationCenter.default.post(name: MapViewController.PhotoAddedNotification, object: nil)
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .createPhotoAction
            .errors
            .subscribe(onNext: { actionError in
                self.createPhotoDescriptionTextView.resignFirstResponder()
                self.createPhotoPopupOverlayView.isHidden = true
                self.presentAlertWithActionError(actionError)
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindFilteredCategoriesViewModel() {
        self.filterCategoriesView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { sender in
                self.filterViewModel = FilterCategoryViewModel(filteredCategories: self.viewModel.filteredCategories.value)
                let filterCategoryViewController = FilterCategoryViewController(nibName: FilterCategoryViewController.className, bundle: nil, viewModel: self.filterViewModel)
                self.present(filterCategoryViewController, animated: true)
            })
            .addDisposableTo(self.disposeBag)
        NotificationCenter.default
            .rx
            .notification(FilterCategoryViewController.FilteredCategoriesChangedNotification)
            .map { notification -> Set<Photo.Category> in
                self.dismiss(animated: true)
                return notification.object as! Set<Photo.Category>
            }
            .bindTo(self.viewModel.filteredCategories)
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .filteredPhotosChangedObservable
            .subscribe(onNext: { photoAnnotations in
                let toRemoveCount = self.mapView.annotations.count
                var toRemove = [MKAnnotation]()
                toRemove.reserveCapacity(toRemoveCount)
                for annotation in self.mapView.annotations {
                    if (!annotation.isEqual(self.mapView.userLocation)) {
                        toRemove.append(annotation)
                    }
                }
                self.mapView.removeAnnotations(toRemove)
                self.mapView.addAnnotations(photoAnnotations)
            })
            .addDisposableTo(self.disposeBag)
    }
   
    private func bindPinPhotoLoadingViewModel() {
        let deselectAnnotationViewObservable = self.mapView.rx.didDeselectAnnotationView.filter { (mapView: MKMapView, annotationView: MKAnnotationView) -> Bool in
            return annotationView.annotation is PhotoAnnotation
        }
        deselectAnnotationViewObservable
            .subscribe(onNext: { (mapView: MKMapView, annotationView: MKAnnotationView) in
                let photoView = annotationView as? PhotoPinAnnotationView
                photoView!.removeGestureRecognizer(self.selectedPinTapGestureRecognizer)
            })
            .addDisposableTo(self.disposeBag)
        var pinAnnotationView: PhotoPinAnnotationView? = nil
        self.mapView
            .rx
            .didSelectAnnotationView
            .filter { (mapView: MKMapView, annotationView: MKAnnotationView) -> Bool in
                return annotationView.annotation is PhotoAnnotation
            }
            .flatMap { (mapView: MKMapView, annotationView: MKAnnotationView) -> Observable<Data> in
                pinAnnotationView = annotationView as? PhotoPinAnnotationView
                let leftView = pinAnnotationView!.leftCalloutAccessoryView as! UIImageView
                leftView.image = UIImage(named:"PlaceholderIcon")
                let photoAnnotation = pinAnnotationView!.annotation as! PhotoAnnotation
                var region = self.mapView.region
                region.center.latitude = photoAnnotation.photo.latitude.doubleValue
                region.center.longitude = photoAnnotation.photo.longitude.doubleValue
                self.mapView.setRegion(region, animated: true)
                pinAnnotationView!.addGestureRecognizer(self.selectedPinTapGestureRecognizer)
                return AppDelegate.shared().reachability.isReachable ? photoAnnotation.photo.downloadPictureThumbnailObservable().takeUntil(deselectAnnotationViewObservable) : Observable<Data>.just(UIImageJPEGRepresentation(leftView.image!, 1.0)!)
            }
            .subscribe(onNext: { pictureData in
                let leftView = pinAnnotationView!.leftCalloutAccessoryView as! UIImageView
                leftView.image = UIImage(data: pictureData)
            }, onError: { error in
                self.presentAlertWithMessage(error.localizedDescription)
            })
            .addDisposableTo(self.disposeBag)
    }

    private func bindSelectedPhotoCategoryViewModel() {
        let categoryViewAppearsObservable = self.selectedCategoryView
            .rx
            .tap
            .asObservable().map { sender -> Bool in
                return false
        }
        let categoryViewDisppearsObservable = self.categoryPickerDoneView
            .rx
            .tap
            .asObservable().map { sender -> Bool in
                return true
        }
        Observable<Bool>.merge([categoryViewAppearsObservable, categoryViewDisppearsObservable])
            .bindTo(self.categoryPickerOverlay.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        self.categoryPickerView
            .rx
            .didSelectRowInComponent
            .subscribe(onNext: { (pickerView: UIPickerView, row: Int, component: Int) in
                self.setupSelectedCategory(self.viewModel.categories[row])
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindActivityExecuting() {
        self.viewModel
            .loadPhotosAction
            .elements
            .bindTo(self.viewModel.photos)
            .addDisposableTo(self.disposeBag)
        let createPhotoExecutingObservable = self.viewModel
            .createPhotoAction
            .executing
            .map { isExecuting -> Bool in
                return !isExecuting
        }
        let loadPhotosExecutingObservable = self.viewModel
            .loadPhotosAction
            .executing
            .map { isExecuting -> Bool in
                return !isExecuting
        }
        let executingObservable = Observable<Bool>.merge([createPhotoExecutingObservable, loadPhotosExecutingObservable])
        executingObservable.bindTo(self.mapActivityIndicatorView.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        executingObservable.bindTo(self.view.rx.isUserInteractionEnabled)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindKeyboardAppearing() {
        NotificationCenter.default
            .rx
            .notification(Notification.Name.UIKeyboardWillShow)
            .subscribe(onNext: { notification in
                let keyboardFrameBegin = notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as! NSValue
                var frame = self.createPhotoPopupView.frame
                if notification.userInfo?[UIKeyboardAnimationCurveUserInfoKey] as! Int != 0 {
                    frame.origin.y = self.view.frame.size.height - keyboardFrameBegin.cgRectValue.size.height - frame.size.height
                    self.createPhotoPopupView.frame = frame
                }
            })
            .addDisposableTo(self.disposeBag)
        NotificationCenter.default
            .rx
            .notification(Notification.Name.UIKeyboardWillHide)
            .subscribe(onNext: { notification in
                let keyboardFrameEnd = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as! NSValue
                var frame = self.createPhotoPopupView.frame
                frame.origin.y = (self.view.frame.size.height - keyboardFrameEnd.cgRectValue.size.height) / 2
                self.createPhotoPopupView.frame = frame
            })
            .addDisposableTo(self.disposeBag)
    }
    
    // MARK: - UIPickerViewDataSource
    
    public func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }

    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return self.viewModel.categories.count
    }
    
    // MARK: - UIPickerViewDelegate
    
    public func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        return pickerView.bounds.size.width
    }
    
    public func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return MapViewController.PickerViewCellHeight
    }
    
    public func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var categoryView = view as? CategoryView
        if categoryView == nil {
            categoryView = CategoryView(frame: CGRect(x: 0.0, y: 0.0, width: pickerView.bounds.size.width, height: MapViewController.PickerViewCellHeight))
        }
        let category = self.viewModel.categories[row]
        categoryView!.categoryTextView.text = category.rawValue
        categoryView!.categoryIconView.image = PhotoPinAnnotationView.imageIconForCategory(category)
        
        return categoryView!;
    }
    
    // MARK: - MKMapViewDelegate
    
    public func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        } else if annotation is PhotoAnnotation {
            let reuseId = PhotoPinAnnotationView.className
            var pinAnnotationView = self.mapView.dequeueReusableAnnotationView(withIdentifier: reuseId) as! PhotoPinAnnotationView?
            if let annotationView = pinAnnotationView {
                annotationView.updateViewWithAnnotation(annotation)
            } else {
                pinAnnotationView = PhotoPinAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            }
            return pinAnnotationView;
        }
        return nil
    }
    
    // MARK: - UITextViewDelegate
    
    public func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        
        return true
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
