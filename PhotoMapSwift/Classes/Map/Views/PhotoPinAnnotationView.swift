//
//  PhotoPinAnnotationView.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/23/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import MapKit

class PhotoPinAnnotationView: MKAnnotationView {

    private static let CalloutAccessoryViewWidth = 55.0
    private static let CalloutAccessoryViewHeight = 55.0
    
    public class func imageIconForCategory(_ category: Photo.Category) -> UIImage? {
        switch category {
        case .Default:
            return UIImage(named: "MarkerDefaultIcon")
        case .Friends:
            return UIImage(named: "MarkerFriendsIcon")
        case .Nature:
            return UIImage(named: "MarkerNatureIcon")
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        
        self.canShowCallout = true
        let photoView = UIImageView(frame: CGRect(x: 0.0, y: 0.0, width: PhotoPinAnnotationView.CalloutAccessoryViewWidth, height: PhotoPinAnnotationView.CalloutAccessoryViewHeight))
        photoView.contentMode = .scaleAspectFit
        self.leftCalloutAccessoryView = photoView
        let rightButton = UIButton(type: .custom)
        rightButton.setImage(UIImage(named: "ArrowRightIcon"), for: .normal)
        rightButton.sizeToFit()
        rightButton.isUserInteractionEnabled = false
        self.rightCalloutAccessoryView = rightButton
        self.updateViewWithAnnotation(annotation)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    public func updateViewWithAnnotation(_ annotation: MKAnnotation?) {
        self.annotation = annotation;
        let photoAnnotation = annotation as! PhotoAnnotation
        self.image = PhotoPinAnnotationView.imageIconForCategory(photoAnnotation.photo.category)
        if let imageHeight = self.image?.size.height {
            self.calloutOffset = CGPoint(x: 0.0, y: imageHeight / 2.0)
        }
    }
}
