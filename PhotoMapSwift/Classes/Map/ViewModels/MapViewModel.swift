//
//  MapViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/20/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import Action
import CoreLocation
import Firebase
import FirebaseAuth
import FirebaseDatabase

class MapViewModel: NSObject {
    
    private static let MaxThumbnailSizeLength: CGFloat = 100.0
    
    var photoLocation = Variable<CLLocation?>(nil)
    var photoCreatedDate: Date?
    var photoDescriptionText = Variable<String?>(nil)
    var selectedCategory = Photo.Category.Default
    let categories: [Photo.Category] = [.Default, .Friends, .Nature]
    var filteredCategories = Variable<Set<Photo.Category>>([.Default, .Friends, .Nature])
    var photos = Variable<[Photo]>([])
    private var locationManager = CLLocationManager()
    private var photoData: Data?
    private var photoThumbnailData: Data?
    var createPhotoAction: Action<Void, PhotoAnnotation?>!
    var loadPhotosAction: Action<Void, [Photo]>!
    var filteredPhotosChangedObservable: Observable<[PhotoAnnotation]>!
    
    override init() {
        super.init()
        
        self.initialize()
    }

    public func requestUserLocationAuthorization() {
        self.locationManager.requestWhenInUseAuthorization()
    }
    
    public func formatPhotoCreatedDate() -> String {
        return String(format: DateManager.sharedInstance.MMMM_d_suffix_comma_yyyy_hyphen_h_mm_a.string(from: self.photoCreatedDate!), DateManager.daySuffixForDate(self.photoCreatedDate!))
    }
    
    private func initialize() {
        let actionActiveObservable = AppDelegate.shared().reachableObservable()
        self.createPhotoAction = Action<Void, PhotoAnnotation?>(enabledIf: actionActiveObservable, workFactory: { sender -> Observable<PhotoAnnotation?> in
            return self.createIndexedPhoto()
        })
        self.loadPhotosAction = Action<Void, [Photo]>(enabledIf: actionActiveObservable, workFactory: { sender -> Observable<[Photo]> in
            return Photo.readByOwnerId(FIRAuth.auth()!.currentUser!.uid)
        })
        self.filteredPhotosChangedObservable = Observable<[PhotoAnnotation]>.combineLatest(self.filteredCategories.asObservable(), self.photos.asObservable(), resultSelector: { filteredCategories, photos -> [PhotoAnnotation] in
            var photoAnnotations = [PhotoAnnotation]()
            photoAnnotations.reserveCapacity(photos.count)
            for photo in photos {
                if (filteredCategories.contains(photo.category)) {
                    photoAnnotations.append(PhotoAnnotation(photo: photo))
                }
            }
            return photoAnnotations
        })
    }
    
    private func createIndexedPhoto() -> Observable<PhotoAnnotation?> {
        let tags = Hashtag.parseTagsInText(self.photoDescriptionText.value!)
        var createIndexedPhotoObservable = self.createPhoto()
        if (tags.count > 0) {
            var photo: Photo! = nil
            createIndexedPhotoObservable = createIndexedPhotoObservable
                .flatMapLatest { createdPhoto -> Observable<[Hashtag]> in
                    photo = createdPhoto
                    return Hashtag.readAll()
            }.flatMapLatest{ hashtags -> Observable<Photo> in
                var photoIndexesObservable = Observable<Photo>.empty()
                for tag in tags {
                    let hashtag = Hashtag.findHashtag(hashtags, withTag: tag)
                    if let hashtag = hashtag {
                        photoIndexesObservable = photoIndexesObservable
                            .concat(PhotoIndex.create(withPhotoKey: photo.key!, withHashtagKey: hashtag.key!)
                                .map({ photoIndex -> Photo in
                                    return photo
                                }))
                    } else {
                        photoIndexesObservable = photoIndexesObservable
                            .concat(Hashtag.create(tag)
                                .flatMapLatest({ hashtag -> Observable<Photo> in
                                    return PhotoIndex.create(withPhotoKey: photo.key!, withHashtagKey: (hashtag?.key!)!)
                                        .map({ photoIndex -> Photo in
                                            return photo
                                        })
                                }))
                    }
                }
                return photoIndexesObservable
            }
        }
        return createIndexedPhotoObservable.map({ photo -> PhotoAnnotation? in
            self.clearPhotoData()
            return PhotoAnnotation(photo: photo)
        })
    }
    
    private func createPhoto() -> Observable<Photo> {
        let createPhotoObservable = Photo.create(ownerId: FIRAuth.auth()!.currentUser!.uid, pictureData: self.photoData, pictureThumbnailData: self.photoThumbnailData, createdTime: self.photoCreatedDate!, category: self.selectedCategory, descriptionText: self.photoDescriptionText.value!, latitude: NSNumber(value: self.photoLocation.value!.coordinate.latitude), longitude: NSNumber(value: self.photoLocation.value!.coordinate.longitude))
            .flatMapLatest { photo -> Observable<Photo> in
                let photo = photo!
                return photo.uploadPictureObservable()
            }.flatMapLatest { photo -> Observable<Photo> in
                return photo.uploadPictureThumbnailObservable()
            }
        
        return createPhotoObservable
    }
    
    public func setPhotoDataWith(originalImage image: UIImage) {
        if let thumbnailImage = image.resizeImageWithMaxSizeLength(MapViewModel.MaxThumbnailSizeLength) {
            self.photoData = UIImageJPEGRepresentation(thumbnailImage, 1.0)
            self.photoThumbnailData = UIImageJPEGRepresentation(thumbnailImage, 0.5)
        }
    }
    
    public func getCurrentPhoto() -> Photo {
        let photo = Photo(ownerId: FIRAuth.auth()!.currentUser!.uid, pictureData: self.photoData, pictureThumbnailData: self.photoThumbnailData, createdTime: self.photoCreatedDate!, category: self.selectedCategory, descriptionText: self.photoDescriptionText.value!, latitude: NSNumber(value: self.photoLocation.value!.coordinate.latitude), longitude: NSNumber(value: self.photoLocation.value!.coordinate.longitude))
        return photo
    }
    
    public func clearPhotoData() {
        self.photoLocation.value = nil
        self.photoData = nil
        self.photoThumbnailData = nil
        self.photoCreatedDate = nil
        self.selectedCategory = .Default
        self.photoDescriptionText.value = ""
    }
}
