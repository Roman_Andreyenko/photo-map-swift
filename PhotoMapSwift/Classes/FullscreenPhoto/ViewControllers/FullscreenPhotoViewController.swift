//
//  FullscreenPhotoViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 4/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit

class FullscreenPhotoViewController: BaseViewController, UIScrollViewDelegate {

    @IBOutlet private weak var scrollView: UIScrollView!
    @IBOutlet private weak var backButtonView: UIButton!
    @IBOutlet private weak var topBarView: UIView!
    @IBOutlet private weak var bottomBarView: UIView!
    @IBOutlet private weak var photoDescriptionView: UILabel!
    @IBOutlet private weak var photoCreationDateView: UITextField!
    @IBOutlet private weak var photoView: UIImageView!
    @IBOutlet private weak var activityIndicatorView: UIActivityIndicatorView!
    
    @IBOutlet private weak var hideTopBottomBarTapGestureRecognizer: UITapGestureRecognizer!
    @IBOutlet private weak var zoomPhotoTapGestureRecognizer: UITapGestureRecognizer!
    
    private weak var viewModel: FullscreenPhotoViewModel!
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: FullscreenPhotoViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    // MARK: - UI logic methods
    
    private func setupViews() {
        self.setupShadowFor(view: self.topBarView, withOffsetSize: CGSize(width: 0.0, height: 4.0))
        self.setupShadowFor(view: self.bottomBarView, withOffsetSize: CGSize(width: 0.0, height: -4.0))
        self.hideTopBottomBarTapGestureRecognizer.require(toFail: self.zoomPhotoTapGestureRecognizer)
        self.scrollView.delegate = self
    }
    
    private func setupShadowFor(view: UIView, withOffsetSize offsetSize: CGSize) {
        view.layer.masksToBounds = false
        view.layer.shadowOffset = offsetSize
        view.layer.shadowRadius = 2.0
        view.layer.shadowOpacity = 0.9
        view.layer.shadowColor = UIColor.black.cgColor
    }
    
    private func hideBars() {
        let sourceTopBarHidden = self.topBarView.isHidden
        let sourceBottomBarHidden = self.bottomBarView.isHidden
        var sourceTopBarFrame = self.topBarView.frame
        var destinationTopBarFrame = self.topBarView.frame
        var sourceBottomBarFrame = self.bottomBarView.frame
        var destinationBottomBarFrame = self.bottomBarView.frame
        if (sourceTopBarHidden) {
            sourceTopBarFrame.origin.y -= sourceTopBarFrame.size.height
        } else {
            destinationTopBarFrame.origin.y -= destinationTopBarFrame.size.height
        }
        if (sourceBottomBarHidden) {
            sourceBottomBarFrame.origin.y += sourceBottomBarFrame.size.height
        } else {
            destinationBottomBarFrame.origin.y += destinationBottomBarFrame.size.height
        }
        self.topBarView.isHidden = false
        self.topBarView.frame = sourceTopBarFrame;
        self.bottomBarView.isHidden = false
        self.bottomBarView.frame = sourceBottomBarFrame;
        self.hideTopBottomBarTapGestureRecognizer.isEnabled = false
        UIView.animate(withDuration: 0.3, animations: { 
            self.topBarView.frame = destinationTopBarFrame
            self.bottomBarView.frame = destinationBottomBarFrame
        }) { isFinished in
            self.hideTopBottomBarTapGestureRecognizer.isEnabled = true
            self.topBarView.isHidden = !sourceTopBarHidden
            self.topBarView.frame = (sourceTopBarHidden) ? destinationTopBarFrame : sourceTopBarFrame
            self.bottomBarView.isHidden = !sourceBottomBarHidden
            self.bottomBarView.frame = (sourceBottomBarHidden) ? destinationBottomBarFrame : sourceBottomBarFrame
        }
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bindPhotoViews()
        self.bindBarsAppearing()
        self.bindPhotoZoom()
        self.bindPhotoLoading()
    }

    private func bindPhotoViews() {
        self.backButtonView
            .rx
            .tap
            .asObservable()
            .subscribe(onNext: { sender in
                self.dismissHorizontallyViewController(animated: false)
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .textDescriptionChangedObservable
            .bindTo(self.photoDescriptionView.rx.text)
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .creationDateChangedObservable
            .bindTo(self.photoCreationDateView.rx.text)
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindBarsAppearing() {
        self.hideTopBottomBarTapGestureRecognizer
            .rx
            .event
            .asObservable()
            .subscribe(onNext: { gestureRecognizer in
                self.hideBars()
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPhotoZoom() {
        self.zoomPhotoTapGestureRecognizer
            .rx
            .event
            .asObservable()
            .subscribe(onNext: { gestureRecognizer in
                self.scrollView.setZoomScale(min(self.scrollView.maximumZoomScale, self.scrollView.zoomScale * 2.0), animated: true)
                
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bindPhotoLoading() {
        self.viewModel
            .photoLoadingAction
            .elements
            .subscribe(onNext: { photoImage in
                self.photoView.image = photoImage
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .photoLoadingAction
            .errors
            .subscribe(onNext: { actionError in
                self.presentAlertWithActionError(actionError)
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .photoLoadingAction
            .executing
            .map { isExecuting -> Bool in
                return !isExecuting
            }
            .bindTo(self.activityIndicatorView.rx.isHidden)
            .addDisposableTo(self.disposeBag)
        AppDelegate.shared()
            .reachableObservable()
            .subscribe(onNext: { isReachable in
                if isReachable {
                    self.viewModel.photoLoadingAction.execute(self.rx.deallocating)
                } else {
                    self.presentSettingsAlertWith(message: NSLocalizedString("NoInternetConnection", comment: ""))
                }
            })
            .addDisposableTo(self.disposeBag)
    }
    
    // MARK: - UIScrollViewDelegate
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return self.photoView
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
