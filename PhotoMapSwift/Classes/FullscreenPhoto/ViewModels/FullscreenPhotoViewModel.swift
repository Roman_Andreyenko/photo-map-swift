//
//  FullscreenPhotoViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 4/4/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import Action

class FullscreenPhotoViewModel: NSObject {

    var photoLoadingAction: Action<Observable<Void>, UIImage?>!
    var photo: Variable<Photo>!
    var textDescriptionChangedObservable: Observable<String>!
    var creationDateChangedObservable: Observable<String>!
    
    init(photo: Photo) {
        self.photo = Variable(photo)
        self.photoLoadingAction = Action<Observable<Void>, UIImage?>(workFactory: { input -> Observable<UIImage?> in
            return photo.downloadPictureObservable().takeUntil(input).map { data -> UIImage? in
                return UIImage(data: data)
            }
        })
        self.textDescriptionChangedObservable = self.photo.asObservable().map { photo -> String in
            return photo.descriptionText
        }
        self.creationDateChangedObservable = self.photo.asObservable().map { photo -> String in
            return String(format: DateManager.sharedInstance.MMMM_d_suffix_comma_yyyy_at_h_mm_a.string(from: photo.createdTime), DateManager.daySuffixForDate(photo.createdTime))
        }
    }
}
