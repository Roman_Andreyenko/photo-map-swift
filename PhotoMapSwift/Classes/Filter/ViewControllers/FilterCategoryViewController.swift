//
//  FilterCategoryViewController.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 4/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import RxCocoa
import RxSwift

class FilterCategoryViewController: BaseViewController {
    
    public static let FilteredCategoriesChangedNotification = NSNotification.Name("FilteredCategoriesChangedNotification")
    
    @IBOutlet private weak var navigationBar: UINavigationBar!
    @IBOutlet private weak var natureCategoryView: UIButton!
    @IBOutlet private weak var friendsCategoryView: UIButton!
    @IBOutlet private weak var defaultCategoryView: UIButton!
    
    private weak var viewModel: FilterCategoryViewModel!
    
    // MARK: - init & deinit
    
    init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?, viewModel: FilterCategoryViewModel) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.viewModel = viewModel
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: - UIViewController lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setupViews()
        self.bindViewModel()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UI logic methods
    
    private func setupViews() {
        self.configure(categoryView: self.natureCategoryView, withBorderColor: UIColor.natureCategoryColor())
        self.configure(categoryView: self.friendsCategoryView, withBorderColor: UIColor.friendsCategoryColor())
        self.configure(categoryView: self.defaultCategoryView, withBorderColor: UIColor.defaultCategoryColor())
    }
    
    private func configure(categoryView: UIButton, withBorderColor borderColor: UIColor) {
        categoryView.layer.cornerRadius = categoryView.bounds.size.height / 2.0
        categoryView.layer.borderWidth = 1.0
        categoryView.layer.borderColor = borderColor.cgColor
    }
    
    // MARK: - Binding
    
    private func bindViewModel() {
        self.bind(filterView: self.natureCategoryView, withColor: UIColor.natureCategoryColor(), withCategory: .Nature)
        self.bind(filterView: self.friendsCategoryView, withColor: UIColor.friendsCategoryColor(), withCategory: .Friends)
        self.bind(filterView: self.defaultCategoryView, withColor: UIColor.defaultCategoryColor(), withCategory: .Default)
        self.navigationBar
            .topItem?
            .rightBarButtonItem?
            .rx
            .tap
            .subscribe(onNext: { sender in
                self.viewModel.filterAction.execute()
            })
            .addDisposableTo(self.disposeBag)
        self.viewModel
            .filterAction
            .elements
            .subscribe(onNext: { filteredCategories in
                NotificationCenter.default.post(name: FilterCategoryViewController.FilteredCategoriesChangedNotification, object: filteredCategories)
            })
            .addDisposableTo(self.disposeBag)
    }
    
    private func bind(filterView: UIButton, withColor filterColor: UIColor, withCategory category: Photo.Category) {
        filterView.rx
            .tap
            .asObservable()
            .subscribe(onNext: { sender in
                filterView.isSelected = !filterView.isSelected
            })
            .addDisposableTo(self.disposeBag)
        filterView.isSelected = self.viewModel.filteredCategories.contains(category)
        filterView.rx
            .observeWeakly(Bool.self, #keyPath(UIButton.selected))
            .subscribe(onNext: { isSelected in
                let isSelected = isSelected ?? false
                self.viewModel.updateFilter(isSelected: isSelected, withCategory: category)
                filterView.backgroundColor = isSelected ? filterColor : UIColor.white
            })
            .addDisposableTo(self.disposeBag)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
