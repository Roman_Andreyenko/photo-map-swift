//
//  FilterCategoryViewModel.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 4/3/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import RxCocoa
import RxSwift
import Action

class FilterCategoryViewModel: NSObject {

    var filteredCategories: Set<Photo.Category>!
    
    var filterAction: Action<Void, Set<Photo.Category>>!
    
    init(filteredCategories: Set<Photo.Category>) {
        super.init()
        
        self.initialize(withFilteredCategories: filteredCategories)
    }
    
    public func updateFilter(isSelected: Bool, withCategory category: Photo.Category) {
        if isSelected {
            self.filteredCategories.insert(category)
        } else {
            self.filteredCategories.remove(category)
        }
    }
    
    private func initialize(withFilteredCategories filteredCategories: Set<Photo.Category>) {
        self.filteredCategories = Set<Photo.Category>(Array<Photo.Category>(filteredCategories))
        
        self.filterAction = Action<Void, Set<Photo.Category>>(workFactory: { input -> Observable<Set<Photo.Category>> in
            return Observable<Set<Photo.Category>>.create { observer -> Disposable in
                observer.onNext(self.filteredCategories)
                observer.onCompleted()
                
                return Disposables.create()
            }
        })
    }
}
