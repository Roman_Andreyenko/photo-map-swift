//
//  AppDelegate.swift
//  PhotoMapSwift
//
//  Created by Roman Andreyenko on 3/6/17.
//  Copyright © 2017 iTechArt. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import ReachabilitySwift
import RxCocoa
import RxSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var reachability: Reachability!
    
    private var welcomeViewModel: WelcomeViewModel!
    private var logOutViewModel: LogOutViewModel!
    private var mapViewModel: MapViewModel!
    private var timelineViewModel: TimelineViewModel!

    class func shared() -> AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
    
    public func reachableObservable() -> Observable<Bool> {
        let reachableObservable = Observable<Bool>.create { observer in
            observer.onNext(self.reachability.isReachable)
            return Disposables.create()
        }
        return Observable<Bool>.merge([reachableObservable, NotificationCenter.default.rx.notification(ReachabilityChangedNotification).map { notification in
            let reachability = notification.object as! Reachability
            return reachability.isReachable
            }])
    }
    
    func setupInitScreenViewController() -> UIViewController {
        self.welcomeViewModel = WelcomeViewModel()
        let welcomeViewController = WelcomeViewController(nibName: WelcomeViewController.className, bundle: nil, viewModel: self.welcomeViewModel)
        let navigationController = UINavigationController(rootViewController: welcomeViewController)
        
        return navigationController
    }
    
    func setupMainScreenViewController() -> UIViewController {
        self.mapViewModel = MapViewModel()
        let mapViewController = MapViewController(nibName: MapViewController.className, bundle: nil, viewModel: self.mapViewModel)
        mapViewController.title = NSLocalizedString("Map", comment: "")
        mapViewController.tabBarItem.image = UIImage(named: "MapIcon.png")
        
        self.timelineViewModel = TimelineViewModel()
        let timelineViewController = TimelineViewController(nibName: TimelineViewController.className, bundle: nil, viewModel: self.timelineViewModel)
        let timelineTabNavigationController = UINavigationController(rootViewController: timelineViewController)
        timelineTabNavigationController.title = NSLocalizedString("Timeline", comment: "")
        timelineTabNavigationController.tabBarItem.image = UIImage(named: "TimelineIcon.png")
        
        self.logOutViewModel = LogOutViewModel()
        let moreViewController = LogOutViewController(nibName: LogOutViewController.className, bundle: nil, viewModel: self.logOutViewModel)
        moreViewController.title = NSLocalizedString("More", comment: "")
        moreViewController.tabBarItem.image = UIImage(named: "MoreIcon.png")
        
        let tabBarController = UITabBarController()
        tabBarController.viewControllers = [mapViewController, timelineTabNavigationController, moreViewController]
        
        return tabBarController
    }
    
    public func setupRootViewController(animated: Bool) {
        let viewController = FIRAuth.auth()?.currentUser != nil ? setupMainScreenViewController() : setupInitScreenViewController()
        self.window?.rootViewController = viewController
        self.window?.makeKeyAndVisible()
        
        if (animated) {
            self.window?.rootViewController?.view.alpha = 0.0;
            
            UIView.animate(withDuration: 1.0, animations: {
                self.window?.rootViewController?.view.alpha = 1.0
            })
        }
    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        FIRApp.configure()
        self.reachability = Reachability()
        do {
            try self.reachability.startNotifier()
        } catch {
            print("could not start reachability notifier")
        }
        
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.backgroundColor = UIColor.white
        setupRootViewController(animated: false)
        
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.reachability.stopNotifier()
    }


}

